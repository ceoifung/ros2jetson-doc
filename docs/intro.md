---
sidebar_position: 0
---

# 目录

<!-- ![](https://kepu.ustc.edu.cn/_upload/article/images/7e/5a/c91b2adc42dd95813403f79bc5b7/f8326d6e-1185-40ff-828c-96ec8f6dbf14.png) -->

<!-- ```jsx
import {ProjectCard} from "../src/components/Project/index.js"

<ProjectCard link="./openwrt-doc/deploy_uboot">测试</ProjectCard>
```
-->
import {ProjectDoubleCard, ProjectCard} from "../src/components/Project/index.js"

<ProjectDoubleCard
     children={{
        title: "基础教程",
        description: "ROS2各款车型硬件参数介绍",
        link: "category/基础教程"
    }}
    children1={{
        title: "进阶教程",
        description: "远程操控以及ROS2小车使用",
        link: "category/进阶教程"
    }}
></ProjectDoubleCard>

<ProjectDoubleCard
    children={{
        title: "二次开发教程",
        description: "ROS2小车的快速开发入门教程",
        link: "category/二次开发教程"
    }}
    children1={{
        title: "ROS2小车的原理以及架构",
        description: "小车的通讯原理以及架构介绍",
        link: "category/ROS2小车的原理以及架构"
    }}
></ProjectDoubleCard>
<ProjectDoubleCard
    children={{
        title: "常见问题与解答",
        description: "使用本款小车遇到问题以及解答",
        link: "category/常见问题与解答"
    }}
    children1={{
        title: "联系我们",
        description: "售后以及反馈方式",
        link: "category/联系我们"
    }}
></ProjectDoubleCard>

<!-- <ProjectDoubleCard
    children={{
        title: "私有maven仓库搭建",
        description: "如何搭建私服",
        link: "./category/私有maven仓库搭建"
    }}
    children1={{
        title: "个人仓库",
        description: "个人开发的一些常用组件",
        link: "./category/个人仓库"
    }}
></ProjectDoubleCard> -->

<!-- 庄子曰：吾生也有涯，而知也无涯。以有涯随无涯，殆已！已而为知者，殆而已矣！为善无近名，为恶无近刑，缘督以为经，可以保身，可以全生，可以养亲，可以尽年。

子夏曰：日知其所亡，月无忘其所能，可谓好学也已矣。

子曰：学而时习之，不亦乐乎

老子曰：合抱之木，生于毫末；九层之台，起于累土；千里之行，始于足下 -->