// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');
const path = require('path')
/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'XiaoRGEEK ROS2 Document',
  tagline: '日知其所亡，月无忘其所能，可谓好学也已矣',
  favicon: 'https://cdn.shopifycdn.net/s/files/1/0144/3568/0342/files/logo_c453a353-f3e6-4173-aea4-7ab3848a0d8e_96x96.jpg',

  // Set the production url of your site here
  url: 'https://ceoifung.gitlab.io',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  // baseUrl: process.env.NODE_ENV == 'development' ? '/' : "/ceoifung/ros2jetson-doc/public",
  baseUrl: '/',
  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'ceoifung', // Usually your GitHub org/user name.
  projectName: 'ceoifung', // Usually your repo name.

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/ceoifung/ros2jetson-doc/blob/master/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/ceoifung/ros2jetson-doc/blob/master/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],
  plugins: [
    // '@aldridged/docusaurus-plugin-lunr',
    require.resolve('docusaurus-lunr-search'),
    // [
    //   require.resolve("@easyops-cn/docusaurus-search-local"),
    //   ({
    //     hashed: true,
    //     indexPages: true
    //   }),
    // ],
    'docusaurus-plugin-image-zoom',
    [
      '@docusaurus/plugin-ideal-image',
      {
        disableInDev: false,
      },
    ],
    [
      '@docusaurus/plugin-pwa',
      {
        debug: true,
        offlineModeActivationStrategies: [
          'appInstalled',
          'standalone',
          'queryString',
        ],
        pwaHead: [
          {
            tagName: 'link',
            rel: 'icon',
            href: 'img/logo.png',
          },
          {
            tagName: 'link',
            rel: 'manifest',
            href: 'manifest.json',
          },
          {
            tagName: 'meta',
            name: 'theme-color',
            content: 'rgb(51 139 255)',
          },
        ],
      },
    ],
  ],
  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'https://cdn.shopifycdn.net/s/files/1/0144/3568/0342/files/logopng_1_255x@2x.png?v=1669193355',
      navbar: {
        title: 'ROS2 JETSON',
        logo: {
          alt: 'My Site Logo',
          src: 'https://cdn.shopifycdn.net/s/files/1/0144/3568/0342/files/logopng_1_255x@2x.png?v=1669193355',
        },
        // 滑动的时候隐藏标题栏
        hideOnScroll: true,
        items: [
          // {
          //   type: 'docSidebar',
          //   sidebarId: 'tutorialSidebar',
          //   position: 'left',
          //   label: 'Document',
          // },
          // { to: '/blog', label: '博客', position: 'left' },
          {
            href: 'https://xiao-r.com',
            label: '官网',
            position: 'right',
          },

          {
            href: 'https://xiaorgeek.github.io/DownloadCenterTemplate',
            label: '软件下载',
            position: 'right',
          },
          {
            href: 'https://gitlab.com/ceoifung/ros2jetson-doc',
            label: 'GitLab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: '文档',
                to: '/docs/intro',
              },
            ],
          },
          {
            title: 'Community',
            items: [
              {
                label: '官网',
                href: 'http://xiao-r.com',
              },
              {
                label: '论坛',
                href: 'http://wifi-robots.com',
              },
              {
                label: '软件下载',
                href: 'https://xiaorgeek.github.io/DownloadCenterTemplate',
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'github',
                href: 'https://github.com/xiaorgeek',
              },
              {
                label: 'GitLab',
                href: 'https://gitlab.com/ceoifung/ros2jetson-doc',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} XiaoRGEEK, Technology Ltd,co. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
        additionalLanguages: ['powershell', 'java', 'kotlin', 'python'],
      },
      tableOfContents: {
        minHeadingLevel: 2,
        maxHeadingLevel: 5,
      },
      zoom: {
        selector: '.markdown :not(em) > img',
        background: {
          light: 'rgb(255, 255, 255)',
          dark: 'rgb(50, 50, 50)',
        },
        config: {},
      },
    }),
};

module.exports = config;
